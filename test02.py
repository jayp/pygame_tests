'''
Created on Feb 2, 2013

@author: PIERREPONT
'''
import sys, pygame
from pygame.locals import *

screen_width = 640
screen_height = 480
screen_size = (screen_width, screen_height)
BLACK = (0, 0, 0)
YELLOW = (255, 255, 0)
pacx = 320
pacy = 50

FPS = 30
speed = 5

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'


pygame.init()

game_clock = pygame.time.Clock()

screen = pygame.display.set_mode(screen_size)
screen.fill(BLACK)
pygame.display.set_caption('Test Game')

pac_radius = 16
pacman = pygame.draw.circle(screen, YELLOW, (pacx, pacy), pac_radius)

direction = None
pressed_keys = []

while True:
    
    for event in pygame.event.get():
        if event.type == QUIT:
            sys.exit()

        if event.type == KEYDOWN:
            if event.key == K_DOWN:
                pressed_keys.append('DOWN')
            elif event.key == K_UP:
                pressed_keys.append('UP')
            elif event.key == K_RIGHT:
                pressed_keys.append('RIGHT')
            elif event.key == K_LEFT:
                pressed_keys.append('LEFT')
                
        
        if event.type == KEYUP:
            if event.key == K_DOWN:
                pressed_keys.remove('DOWN')
            if event.key == K_UP:
                pressed_keys.remove('UP')
            if event.key == K_RIGHT:
                pressed_keys.remove('RIGHT')
            if event.key == K_LEFT:
                pressed_keys.remove('LEFT')
                

    if len(pressed_keys) == 0:
        direction = None
    else:
        direction = pressed_keys[-1]
        
  
    if direction == 'UP':
        pacy -= speed
    elif direction == 'DOWN':
        pacy += speed
    elif direction =='RIGHT':
        pacx += speed
    elif direction == 'LEFT':
        pacx -= speed
    else:
        pass
        
    
    print direction
    #print 'pacx = %d  pacy = %d' % (pacx, pacy)
        
    screen.fill(BLACK)
    pygame.draw.circle(screen, YELLOW, (pacx, pacy), pac_radius)
    pygame.display.flip()
    game_clock.tick(FPS)
