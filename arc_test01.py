'''
Created on Feb 3, 2013

@author: Jay
'''
import sys, pygame
import math

def deg_to_rad(deg):
    return deg / 180.0 * math.pi

pygame.init()

size = width, height = 320, 240
speed = [2, 2]
black = 0, 0, 0
white = (255, 0, 0)
fps = pygame.time.Clock()

screen = pygame.display.set_mode(size)


while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
        
    screen.fill(black)
    start = deg_to_rad(90)
    end = deg_to_rad(180)
    pygame.draw.arc(screen, white, (50, 50, 100, 100), start, end, 10)
    pygame.display.update()
    fps.tick(30)