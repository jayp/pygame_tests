'''
Created on Feb 2, 2013

@author: PIERREPONT
'''
import sys, pygame
from pygame.locals import *

#BLOCK_SIZE = 
screen_width = 640
screen_height = 480
screen_size = (screen_width, screen_height)
BLACK = (0, 0, 0)
YELLOW = (255, 255, 0)
BLUE = (0, 0, 255)
pacx = 320
pacy = 50

FPS = 30
speed = 5

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'


pygame.init()

game_clock = pygame.time.Clock()

screen = pygame.display.set_mode(screen_size)
screen.fill(BLACK)
pygame.display.set_caption('Test Game')

pac_radius = 16
pacman = pygame.Rect(pacx, pacy, pac_radius * 2, pac_radius *2)
pacman_circle = pygame.draw.circle(screen, YELLOW,
                       (pacx + pac_radius, pacy + pac_radius), pac_radius)

direction = None
pressed_keys = []
wall01 = pygame.Rect(250, 100, 80, 200)
walls = [wall01]

pacman = pygame.Rect(pacx, pacy, pac_radius * 2, pac_radius *2)

def draw_walls():    
    pygame.draw.rect(screen, BLUE, wall01)
def draw_pacman():
    pacman = pygame.Rect(pacx, pacy, pac_radius * 2, pac_radius *2)
    pygame.draw.ellipse(screen, YELLOW, pacman)
draw_walls()
draw_pacman()    


while True:
    
    for event in pygame.event.get():
        if event.type == QUIT:
            sys.exit()

        if event.type == KEYDOWN:
            if event.key == K_DOWN:
                pressed_keys.append('DOWN')
            elif event.key == K_UP:
                pressed_keys.append('UP')
            elif event.key == K_RIGHT:
                pressed_keys.append('RIGHT')
            elif event.key == K_LEFT:
                pressed_keys.append('LEFT')
                
        
        if event.type == KEYUP:
            if event.key == K_DOWN:
                pressed_keys.remove('DOWN')
            if event.key == K_UP:
                pressed_keys.remove('UP')
            if event.key == K_RIGHT:
                pressed_keys.remove('RIGHT')
            if event.key == K_LEFT:
                pressed_keys.remove('LEFT')
                

    if len(pressed_keys) == 0:
        direction = None
    else:
        direction = pressed_keys[-1]
        
  
    if direction == 'UP':
        #pacy -= speed
        pacman.move(0, -speed)
    elif direction == 'DOWN':
        #pacy += speed
        pacman.move(0, speed)
    elif direction =='RIGHT':
        #pacx += speed
        pacman.move(speed, 0)
    elif direction == 'LEFT':
        #pacx -= speed
        pacman.move(-speed, 0)
    else:
        pass
        
    
    
    if pacy > screen_height + pac_radius:
        pacy = 1 - pac_radius 
    elif pacy < 0 - pac_radius:
        pacy = screen_height + pac_radius - 1
        
    if pacx > screen_width + pac_radius:
        pacx = 1 - pac_radius
    elif pacx < 0 - pac_radius:
        pacx = screen_width + pac_radius - 1
    
    # check for collision with walls
    for wall in walls:
        if pacman.colliderect(wall):
            print 'BOOM!'
        
    screen.fill(BLACK)
    draw_walls()
    #draw_pacman()
    pygame.display.flip()
    game_clock.tick(FPS)
