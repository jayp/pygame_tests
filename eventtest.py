'''
Created on Feb 2, 2013

@author: PIERREPONT
'''
import sys, pygame
from pygame.locals import *

screen_size = (640, 480)
BLACK = (0, 0, 0)
YELLOW = (255, 255, 0)
pacx = 320
pacy = 50

FPS = 30
speed = 5

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'
direction = None

pygame.init()

game_clock = pygame.time.Clock()

screen = pygame.display.set_mode(screen_size)
screen.fill(BLACK)
pygame.display.set_caption('Test Game')

pacman = pygame.draw.circle(screen, YELLOW, (pacx, pacy), 16)

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            sys.exit()

        if event.type == KEYUP:
           pass

        if event.type == KEYDOWN:
            if event.key == K_DOWN:
                direction = DOWN
            elif event.key == K_UP:
                direction = UP


    if direction == UP:
        pacy += speed
        print 'pacx = %d  pacy = %d' % (pacx, pacy)
    elif direction == DOWN:
        pacy -= speed
        print 'pacx = %d  pacy = %d' % (pacx, pacy)

    screen.fill(BLACK)
    pygame.draw.circle(screen, YELLOW, (pacx, pacy), 16)
    pygame.display.flip()
