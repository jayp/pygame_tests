'''
Created on Feb 2, 2013

@author: PIERREPONT
'''
import sys, pygame
from pygame.locals import *

#BLOCK_SIZE = 
screen_width = 640
screen_height = 640
screen_size = (screen_width, screen_height)
block_size = 32
num_blocks = 20
assert screen_width / block_size == num_blocks, 'Board not wide enough.'
assert screen_height / block_size == num_blocks, 'Board not tall enough'
assert block_size % 4 == 0, 'Block size not divisible by 4'

BLACK = (0, 0, 0)
YELLOW = (255, 255, 0)
BLUE = (0, 0, 255)

pacx = 100
pacy = 32
pac_radius = (block_size / 2)

FPS = 30
speed = 8

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

level01 = [
' _______     ______ ',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
':                  |',
' -------     ------ '
]


class Wall(pygame.sprite.Sprite):
    """Wall class. Pacman cannot move through a wall."""
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        
    def draw_wall(self, x_tile, y_tile, wall_type):
        pass
        

class Pacman(pygame.sprite.Sprite):
    images = ['images/yellow_circle.png']
    collisions = []
    direction = None
    dirty = False
    
    def __init__(self, x_pos, y_pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(self.images[0])
        self.rect = self.image.get_rect()
        self.rect.left = x_pos
        self.rect.top = y_pos
        
    def draw(self):
        pygame.image.load(images[0])
        
    def clear(self):
        pygame.draw.rect(screen, BLACK, self.rect)
    
    def move(self):
        if self.direction == None:
            pass
        elif pacman.direction == UP:
            self.rect.move_ip(0, -speed)
        elif self.direction == DOWN:
            self.rect.move_ip(0, speed)
        elif self.direction == RIGHT:
            self.rect.move_ip(speed, 0)
        elif self.direction == LEFT:
            self.rect.move_ip(-speed, 0)
        else:
            pass
        
    def update(self):
        pass
        
    def has_collided(self, collision_list):
        self.collisions = []
        for item in collision_list:
            if self.rect.colliderect(item):
                self.stop(item)
                self.collisions.append(item)
        if len(self.collisions) > 0:
            return True
        else:
            return False
    
    def stop(self, collision_rect):
        if self.direction == 'right':
            self.rect.right = collision_rect.left
        elif self.direction == 'left':
            self.rect.left = collision_rect.right
        elif self.direction == 'up':
            self.rect.top = collision_rect.bottom
        elif self.direction == 'down':
            self.rect.bottom = collision_rect.top
                

pygame.init()

game_clock = pygame.time.Clock()

screen = pygame.display.set_mode(screen_size)
screen.fill(BLACK)
pygame.display.set_caption('PacMan')


pressed_keys = []

#wall01 = pygame.Rect(screen_width / 2 - 150, 150, 300, 100)
walls = [] # all wall rectangles

#  Wall Types
#==============================================================================
#    1. : single_vertical_line
#    2. - single_horizontal_line
#    3. T top_left_corner
#    4. t top_right_corner
#    5. L bottom_left_corner
#    6. l bottom_right_corner


def draw_vertical_wall(x_tile, y_tile, side):
    interval = (block_size / 4)
    offset  = interval * side 
    x_start = (x_tile * block_size) + (offset)
    x_end   =  x_start
    y_start = (y_tile * block_size)
    y_end   =  y_start + block_size
    pygame.draw.line(screen,BLUE, (x_start, y_start), (x_end, y_end) )
    
def draw_horizontal_wall(x_tile, y_tile, side = 0):
    interval = (block_size / 4)
    offset   = interval * side 
    x_start  = (x_tile * block_size)
    x_end    =  x_start + block_size
    y_start  = (y_tile * block_size) + offset
    y_end    =  y_start 
    pygame.draw.line(screen, BLUE, (x_start, y_start), (x_end, y_end) )
    
def draw_top_left_corner(x_tile, y_tile, side):
    interval = (block_size / 4)
    offset  = interval * side
    x_start  = (x_tile * block_size) + offset
    x_corner = x_start
    x_end    =  x_start + (block_size / 2)
    y_start  = (y_tile * block_size) + block_size
    y_corner =  y_start - block_size / 2
    y_end    =  y_corner 
    pygame.draw.arc(screen, BLUE, (x_start, y_start, block_size, block_size),
                    (x_start, y_start),(x_end, y_end))
    
def draw_top_right_corner(x_tile, y_tile, side):
    x_start  = (x_tile * block_size)
    x_corner =  x_start + (block_size / 2)
    x_end    =  x_corner
    y_start  = (y_tile * block_size) + (block_size / 2)
    y_corner = y_start
    y_end    =  y_corner + (block_size / 2)
    pygame.draw.lines(screen, BLUE, False, [(x_start, y_start),
                            (x_corner, y_corner), (x_end, y_end)])
    
def draw_bottom_left_corner(x_tile, y_tile, side):
    x_start  = (x_tile * block_size + block_size / 2)
    x_corner =  x_start
    x_end    =  (x_corner + block_size / 2)
    y_start  = (y_tile * block_size)
    y_corner = (y_start + block_size / 2)
    y_end    =  y_corner
    pygame.draw.lines(screen, BLUE, False, [(x_start, y_start),
                            (x_corner, y_corner), (x_end, y_end)])
    
def draw_bottom_right_corner(x_tile, y_tile, side):
    x_start  = (x_tile * block_size)
    x_corner =  (x_start + block_size / 2)
    x_end    =  x_corner
    y_start  = (y_tile * block_size + block_size / 2)
    y_corner =  y_start
    y_end    =  (y_corner - block_size / 2 )
    pygame.draw.lines(screen, BLUE, False, [(x_start, y_start),
                            (x_corner, y_corner), (x_end, y_end)])
    
wall_types = {
   ":": [draw_vertical_wall, 4],
   "|": [draw_vertical_wall, 0],
   "_": [draw_horizontal_wall, 4],
   "-": [draw_horizontal_wall, 0],
   "T": [draw_top_left_corner, 0],
   "t": [draw_top_right_corner, 0],
   "L": [draw_bottom_left_corner, 0],
   "l": [draw_bottom_right_corner, 0]
   }


def draw_wall(type, x_tile, y_tile):
    if type in wall_types.keys():
        pos = wall_types[type][1]
        wall_types[type][0](x_tile, y_tile, pos)
        wall = pygame.Rect(x_tile * block_size, y_tile * block_size,
                           block_size, block_size)
        walls.append(wall)

def draw_walls(level):
    """Parse level. Determine type of wall. Draw walls.
       level is a list of lists. ( a 2D list)"""
    y_tile = 0
    for y_index in level:
        x_tile = 0
        for x_index in y_index:
            type = x_index
            draw_wall(type, x_tile, y_tile)
            x_tile += 1
        y_tile += 1


draw_walls(level01)
background = screen.copy()
pacman = Pacman(pacx, pacy) 

#Main Game Loop
while True:
    
    for event in pygame.event.get():
        if event.type == QUIT:
            sys.exit()

        if event.type == KEYDOWN:
            if event.key == K_DOWN or event.key == ord('s'):
                pressed_keys.append(DOWN)
            elif event.key == K_UP or event.key == ord('w'):
                pressed_keys.append(UP)
            elif event.key == K_RIGHT or event.key == ord('d'):
                pressed_keys.append(RIGHT)
            elif event.key == K_LEFT or event.key == ord('a'):
                pressed_keys.append(LEFT)
                
        
        if event.type == KEYUP:
            if event.key == K_DOWN or event.key == ord('s'):
                pressed_keys.remove(DOWN)
            if event.key == K_UP or event.key == ord('w'):
                pressed_keys.remove(UP)
            if event.key == K_RIGHT or event.key == ord('d'):
                pressed_keys.remove(RIGHT)
            if event.key == K_LEFT or event.key == ord('a'):
                pressed_keys.remove(LEFT)
                
    if len(pressed_keys) == 0:
        pacman.direction = None
    else:
        pacman.direction = pressed_keys[-1]
    screen.blit(background, pacman.rect, area=pacman.rect)    
    pacman.move()
    
    # If pacman goes off the screen, make him appear at the other end of the screen    
    if pacman.rect.y > screen_height + pac_radius:
        pacman.rect.y = 1 - pac_radius 
    elif pacman.rect.y < 0 - pac_radius:
        pacman.rect.y = screen_height + pac_radius - 1
        
    if pacman.rect.x > screen_width + pac_radius:
        pacman.rect.x = 1 - pac_radius
    elif pacman.rect.x < 0 - pac_radius:
        pacman.rect.x = screen_width + pac_radius - 1
    
    # check for collision with walls
    pacman.has_collided(walls)

    screen.blit(pacman.image, pacman.rect)
    pygame.display.flip()
    game_clock.tick(FPS)
